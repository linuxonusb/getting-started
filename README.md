# Getting Started


_Coming soon..._


## Boot from USB

Insert the USB drive into USB 2.0 or USB 3.0 port, and boot the machine.
You will first need to configure the computer's BIOS/UEFI to boot from a USB device.
(TBD: How to do it?)


## Connect to the Network

There is really no "installation" or setup steps to boot the Linux system
since it's already pre-installed on the USB drive.

Once the system boots up, the very first you need to is to connect to the Interent.
Most likely, this will be done via hoe or office WIFI.
(TBD: how to do it?)


## Change the username and password

The system comes with a default user account with pre-assigned username and password.
It's best to change these as soon as possible.
(TBD: how to do it?)

Then, optionally, you may want to change the computer's "hostname". 
The computer's name reflects your personality. Choose something that is "you".
(TBD: how to do it?)


## Update the system software

The USB drive comes with the most recent system image pre-installed.
But, as a general practice, you may want to try to update the system, when you first boot the system, and on a regular basis.
(TBD: how to do it?)






